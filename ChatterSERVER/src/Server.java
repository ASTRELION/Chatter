import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Server extends JFrame 
{

	private JTextField userInput; //users message area
	private JTextArea chatWindow; //Displaying messages
	
	private ObjectOutputStream output; //Sending message away to another source
	private ObjectInputStream input; //Receiving message from another source
	private ServerSocket server; //Server that is the connection point for all clients
	private Socket connection; //Setting up connection between clients
	
	//GUI
	public Server()
	{
		
		super("Chatter - Server");
		
		chatWindow = new JTextArea();
		chatWindow.setEditable(false);
		
		userInput = new JTextField();
		userInput.setEditable(false);
		userInput.addActionListener
		(
				
			new ActionListener()
			{
					
				public void actionPerformed(ActionEvent event)
				{
						
					sendMessage(event.getActionCommand()); //returning the text in the field
					userInput.setText(""); //When sent, make it nothing
						
				}
					
			}
				
		);
		
		add(userInput, BorderLayout.SOUTH);
		add(new JScrollPane(chatWindow)); //Making a scroll window with the chatWindow displayed in it
		
		setSize(400,500);
		setVisible(true);
		
	}
	
	//set up and run server
	public void start()
	{
		
		try
		{
			
			server = new ServerSocket(6789, 100); //6789 = port //100 = queue length
			
			while(true)
			{
				
				try
				{
					
					waitConnection(); //waiting for clients
					setupStream(); //set up a connection between clients
					whileChatting(); //allows to send message back and forth
					
				}
				catch(EOFException e)
				{
					
					showMessage("\nEND OF STREAM");
					
				}
				finally
				{
					
					close(); //closes all connections and methods
					
				}
				
			}
			
		}
		catch(IOException e)
		{
			
			e.printStackTrace();
			
		}
		
	}
	
	//Wait for connect, connection success msg
	private void waitConnection() throws IOException
	{
		
		showMessage("\nWaiting for connection...");
		connection = server.accept(); //Accepts connection that client requests via app
		showMessage("\nNow connected to " + connection.getInetAddress().getHostName()); //Shows ip address of connected client
		
	}
	
	//Setup stream to get and receive data
	private void setupStream() throws IOException
	{
		
		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush(); //getting rid of extra data (Fail-safe)
		
		input = new ObjectInputStream(connection.getInputStream());
		
		showMessage("\nConnection setup!");
		
	}
	
	//Whilechatting, runs while connected
	public void whileChatting() throws IOException
	{
		
		String message = "You may begin chatting!";
		sendMessage(message);
		ableToType(true);
		
		do
		{
			
			try
			{
				
				message = (String)input.readObject();
				showMessage("\n" + message);
				
			}
			catch(ClassNotFoundException e)
			{
				
				showMessage("\nMessage not understood");
				
			}
			
		}
		while(!message.equals("CLIENT: END"));
		
	}
	
	//Closes all connections
	private void close()
	{
		
		showMessage("\nClosing connections...");
		ableToType(false);
		
		try
		{
			
			output.close();
			input.close();
			connection.close();
			
		}
		catch(IOException e)
		{
			
			e.printStackTrace();
			
		}
		
	}
	
	//Send message to client
	private void sendMessage(String message)
	{
		
		try
		{
			
			output.writeObject("SERVER: " + message); //Makes object and puts it into output stream
			output.flush(); //Clears out random data left behind
			showMessage("\nSERVER: " + message);
			
		}
		catch(IOException e)
		{
			
			chatWindow.append("ERROR: Could not receive server message"); //Append = put in chat area
			
		}
		
	}
	
	//Updates chat window
	private void showMessage(final String text)
	{
		
		SwingUtilities.invokeLater //Updates parts of the GUI (Text updater)
		(
				
			new Runnable()
			{
				
				public void run()
				{
					
					chatWindow.append(text);
					
				}
				
			}
				
		);
		
	}
	
	//Can/Cannot type
	private void ableToType(final boolean canType)
	{
		
		SwingUtilities.invokeLater
		(
				
			new Runnable()
			{
				
				public void run()
				{
					
					userInput.setEditable(canType);
					
				}
				
			}
				
		);
		
	}
	
}
