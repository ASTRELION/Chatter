import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Client extends JFrame
{

	private JTextField userInput;
	private JTextArea chatWindow;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private String message = "";
	private String serverIP;
	private Socket connection;
	
	public Client(String host)
	{
		
		super("Chatter - Client");
		serverIP = host;
		userInput = new JTextField();
		userInput.setEditable(false);
		userInput.addActionListener
		(
				
			new ActionListener()
			{
				
				public void actionPerformed(ActionEvent e)
				{
					
					sendMessage(e.getActionCommand());
					userInput.setText("");
					
				}
				
			}
				
		);
		
		add(userInput, BorderLayout.SOUTH);
		chatWindow = new JTextArea();
		add(new JScrollPane(chatWindow), BorderLayout.CENTER);
		setSize(400,500);
		setVisible(true);
		
	}
	
	//Starts
	public void Start()
	{
		
		try
		{
			
			connectTo();
			setupStream();
			whileChatting();
			
		}
		catch(EOFException e)
		{
			
			showMessage("\nClient ended connection");
			
		}
		catch(IOException e)
		{
			
			e.printStackTrace();
			
		}
		finally
		{
			
			close();
			
		}
		
	}
	
	//Connecting to server
	private void connectTo() throws IOException
	{
		
		showMessage("\nAttempting to connect to server...");
		connection = new Socket(InetAddress.getByName(serverIP), 6789);
		showMessage("\nConnected to: " + connection.getInetAddress().getHostName());
		
	}
	
	//Setup connections
	private void setupStream() throws IOException
	{
		
		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();
		
		input = new ObjectInputStream(connection.getInputStream());
		showMessage("\nConnections setup!");
		
	}
	
	//While connection is active
	private void whileChatting() throws IOException
	{
		
		ableToType(true);
		
		do
		{
			
			try
			{
				
				message = (String)input.readObject();
				showMessage("\n" + message);
				
			}
			catch(ClassNotFoundException e)
			{
				
				showMessage("\nObject type not known");
				
			}
			
		}
		while(!message.equals("SERVER: END"));
		
	}
	
	//Closes connections
	private void close()
	{
		
		showMessage("\nClosing connection...");
		ableToType(false);
		
		try
		{
			
			output.close();
			input.close();
			connection.close();
			
		}
		catch(IOException e)
		{
			
			e.printStackTrace();
			
		}
		
	}
	
	//send to
	private void sendMessage(String message)
	{
		
		try
		{
			
			output.writeObject("CLIENT: " + message);
			output.flush();
			showMessage("\nCLIENT: " + message);
			
		}
		catch(IOException e)
		{
			
			chatWindow.append("\nERROR: Sending message failed");
			
		}
		
	}
	
	//Update chatWindow
	private void showMessage(final String message)
	{
		
		SwingUtilities.invokeLater
		(
				
			new Runnable()
			{
				
				public void run()
				{
					
					chatWindow.append(message);
					
				}
				
			}
				
		);
		
	}
	
	
	//Can/Cannot type
	private void ableToType(final boolean canType)
	{
		
		SwingUtilities.invokeLater
		(
				
			new Runnable()
			{
				
				public void run()
				{
					
					userInput.setEditable(canType);
					
				}
				
			}
				
		);
		
	}
	
}
